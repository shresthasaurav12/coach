import { createRouter, createWebHistory } from 'vue-router';

// coaches components
import CoachDetail from './pages/coaches/CoachDetail.vue';
import CoachList from './pages/coaches/CoachList.vue';
import CoachRegister from './pages/coaches/CoachRegister.vue';

// requests components
import ContactCoach from './pages/requests/ContactCoach.vue';
import RequestRecieved from './pages/requests/RequestsRecieve.vue';
import NotFound from './pages/NotFound.vue';

const router = createRouter({
    history: createWebHistory(),
    routes: [
        { path: '/', redirect: '/coaches' },
        { path: '/coaches', component: CoachList },
        {
            path: '/coaches/:id', component: CoachDetail, props: true, children: [
                { path: 'contact', component: ContactCoach } // /coaches/id/contact
            ]
        },
        { path: '/register', component: CoachRegister },
        { path: '/requests', component: RequestRecieved },
        { path: '/:notFound(.*)', component: NotFound },
    ],
});

export default router;